package kz.ikar.hypephrases.db;

import android.content.Context;
import android.os.Environment;

import android.util.Log;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.File;
import java.util.List;
import java.util.Properties;

import kz.ikar.hypephrases.models.Category;
import kz.ikar.hypephrases.models.DaoMaster;
import kz.ikar.hypephrases.models.DaoSession;
import kz.ikar.hypephrases.models.Pack;
import kz.ikar.hypephrases.models.PackDao;
import kz.ikar.hypephrases.models.Phrase;
import kz.ikar.hypephrases.models.PhraseDao;

/**
 * Created by User on 26.11.2017.
 */

public class DBHelper {
    private static DaoSession daoSession;
    public static String rootDir;

    public static void loadDaoSession(Context context) {
        rootDir = context.getFilesDir() + "/phrases/";
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "hypephrases-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    /***    PACK FUNCTIONS    ***/

    public static List<Pack> getPacks() {
        return daoSession.getPackDao().loadAll();
    }

    public static Pack getPackById(long id) {
        return daoSession.getPackDao().load(id);
    }

    public static long addPack(Pack pack) {
        return daoSession.getPackDao().insert(pack);
    }

    public static void removeAllPacks() {
        List<Pack> packs = daoSession.getPackDao().loadAll();
        for (Pack pack : packs) {
            for (Phrase phrase : pack.getPhrases()) {
                File file = new File(phrase.getFilename());
                file.delete();
            }
        }
        daoSession.getPackDao().deleteAll();
    }

    public static boolean checkIfPackExists(Pack pack) {
        QueryBuilder<Pack> packQueryBuilder = daoSession.queryBuilder(Pack.class);
        packQueryBuilder.where(PackDao.Properties.Id.eq(pack.getId()));
        return packQueryBuilder.count() != 0;
    }

    public static String getLogoOfPackWithId(long id) {
        QueryBuilder<Pack> packQueryBuilder = daoSession.queryBuilder(Pack.class);
        packQueryBuilder.where(PackDao.Properties.Id.eq(id));
        return packQueryBuilder.list().get(0).getLogo();
    }

    public static void removePack(long packId) {
        Pack pack = daoSession.getPackDao().load(packId);
        for (Phrase phrase : pack.getPhrases()) {
            File file = new File(DBHelper.rootDir + phrase.getFilename());
            if (file.exists())
                if (file.delete()) {
                    Log.i("HYPEPHRASES", file.getAbsolutePath() + " deleted");
                } else {
                    Log.i("HYPEPHRASES", file.getAbsolutePath() + " was not deleted");
                }
            daoSession.getPhraseDao().delete(phrase);
        }
        File imgFile = new File(pack.getLogo());
        imgFile.delete();
        pack.delete();
    }

    /***    PACK FUNCTIONS    ***/

    /***    PHRASE FUNCTIONS    ***/

    public static List<Phrase> getPhrases() {
        return daoSession.getPhraseDao().loadAll();
    }

    public static long addPhrase(Phrase phrase) {
        return daoSession.getPhraseDao().insert(phrase);
    }

    public static void removePhrase(Phrase phrase) { daoSession.getPhraseDao().delete(phrase); }

    public static List<Phrase> getPhrases(String part) {
        QueryBuilder<Phrase> phraseQueryBuilder = daoSession.queryBuilder(Phrase.class);
        phraseQueryBuilder.where(PhraseDao.Properties.Text.like("%" + part + "%"));
        return phraseQueryBuilder.list();
    }

    public static List<Phrase> getPhrasesByCategory(String category) {
        QueryBuilder<Phrase> phraseQueryBuilder = daoSession.queryBuilder(Phrase.class);
        phraseQueryBuilder.where(PhraseDao.Properties.Categories.like("%" + category + "%"));
        return phraseQueryBuilder.list();
    }

    /***    PHRASE FUNCTIONS    ***/

    /***    CATEGORY FUNCTIONS  ***/

    public static List<Category> getCategories() {
        return daoSession.getCategoryDao().loadAll();
    }

    public static void updateCategories(List<Category> categories) {
        if (categories != null && categories.size() != 0) {
            daoSession.getCategoryDao().deleteAll();
            daoSession.getCategoryDao().insertInTx(categories);
            /*Category current = daoSession.getCategoryDao().load(category.getId());
            if (current == null) {
                current = new Category();
                current.setId(category.getId());
                current.setName(category.getName());
            }*/
        }
        //daoSession.getCategoryDao().updateInTx(categories);
    }

    /***    CATEGORY FUNCTIONS  ***/
}
