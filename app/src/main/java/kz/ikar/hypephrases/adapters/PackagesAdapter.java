package kz.ikar.hypephrases.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import kz.ikar.hypephrases.BuildConfig;
import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.activities.MainActivity;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.models.AppContext;
import kz.ikar.hypephrases.models.Pack;
import kz.ikar.hypephrases.models.Phrase;

/**
 * Created by User on 19.09.2017.
 */

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.PeopleViewHolder> {
    private List<Pack> packs;
    private AssetManager assetManager;
    private MainActivity mainActivity;
    private OnClickListener listener;

    public interface OnClickListener {
        void onPackDeleteClicked(Pack pack);
    }

    public PackagesAdapter(List<Pack> packs, AssetManager assetManager, MainActivity mainActivity, OnClickListener listener) {
        this.packs = packs;
        this.assetManager = assetManager;
        this.mainActivity = mainActivity;
        this.listener = listener;
    }

    @Override
    public PeopleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new PeopleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PeopleViewHolder holder, int position) {
        holder.bind(packs.get(position), assetManager, position, mainActivity, listener);
    }

    @Override
    public int getItemCount() {
        if (packs != null) {
            return packs.size();
        }

        return 0;
    }

    public static class PeopleViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        RecyclerView rvPhrases;
        ImageView ivLogo;
        Button btnDelete;
        AssetManager assetManager;
        MediaPlayer player;

        public PeopleViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            rvPhrases = (RecyclerView) itemView.findViewById(R.id.rv_phrases);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            btnDelete = (Button) itemView.findViewById(R.id.btn_delete);

            player = AppContext.player;
        }

        public void bind(final Pack pack, AssetManager assetManager, int position, MainActivity mainActivity, final OnClickListener listener) {
            tvName.setText(pack.getName());
            ivLogo.setImageBitmap(getBitmapLogo(pack.getLogo()));
            this.assetManager = assetManager;

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPackDeleteClicked(pack);
                }
            });

            PhrasesAdapter adapter = new PhrasesAdapter(pack.getAllPhrases(), new PhrasesAdapter.OnPhraseClickListener() {
                @Override
                public void onItemClicked(int position) {
                    try {
                        player.reset();
                        Phrase phrase = pack.getAllPhrases().get(position);
                        String path = DBHelper.rootDir + phrase.getFilename();
                        File file = new File(path);
                        if (file.exists()) {
                            player.setDataSource(file.getAbsolutePath());
                        } else {
                            return;
                        }
                        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                            }
                        });
                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.reset();
                            }
                        });
                        player.prepareAsync();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onShareButtonClicked(int position) {
                    String internalFilepath = DBHelper.rootDir + pack.getAllPhrases().get(position).getFilename();
                    Uri uri = FileProvider.getUriForFile(rvPhrases.getContext(),
                        BuildConfig.APPLICATION_ID,
                        new File(internalFilepath));
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("audio/*");
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    rvPhrases.getContext().startActivity(Intent.createChooser(share, "Отправка фразы..."));
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(mainActivity, LinearLayoutManager.HORIZONTAL, false);
            rvPhrases.setLayoutManager(layoutManager);
            rvPhrases.setAdapter(adapter);
        }

        private Bitmap getBitmapLogo(String filepath) {
            File file = new File(filepath);
            return BitmapFactory.decodeFile(file.getAbsolutePath());
        }
    }
}
