package kz.ikar.hypephrases.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.models.Pack;
import kz.ikar.hypephrases.models.Phrase;

/**
 * Created by User on 29.11.2017.
 */

public class SearchPhrasesAdapter extends RecyclerView.Adapter<SearchPhrasesAdapter.SearchPhrasesViewHolder> {
    private List<Phrase> mitemList;
    private OnClickListener listener;

    public interface OnClickListener {
        void OnItemClicked(Phrase phrase);
        void OnItemSended(Phrase phrase);
    }

    public SearchPhrasesAdapter(List<Phrase> mitemList, OnClickListener listener) {
        this.mitemList = mitemList;
        this.listener = listener;
    }

    @Override
    public SearchPhrasesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_phrase_search, parent, false);
        return new SearchPhrasesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchPhrasesViewHolder holder, int position) {
        holder.bind(mitemList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        if (mitemList == null)
            return 0;

        return mitemList.size();
    }

    public static class SearchPhrasesViewHolder extends RecyclerView.ViewHolder {
        TextView tvText;
        TextView tvPack;
        ImageView ivPackLogo;
        CardView cvTile;
        Button btnSend;

        public SearchPhrasesViewHolder(View itemView) {
            super(itemView);

            tvText = (TextView) itemView.findViewById(R.id.tv_text);
            tvPack = (TextView) itemView.findViewById(R.id.tv_pack);
            ivPackLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            cvTile = (CardView) itemView.findViewById(R.id.cv_tile);
            btnSend = (Button) itemView.findViewById(R.id.btn_send);
        }

        public void bind(final Phrase phrase, final OnClickListener listener) {
            tvText.setText(phrase.getText());

            Pack pack = DBHelper.getPackById(phrase.getPackId());
            tvPack.setText(pack.getName());

            File imgFile = new File(pack.getLogo());
            Picasso.with(tvPack.getContext())
                    .load(imgFile)
                    .resize(50, 50)
                    .centerCrop()
                    .into(ivPackLogo);

            cvTile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemClicked(phrase);
                }
            });

            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemSended(phrase);
                }
            });
        }
    }
}
