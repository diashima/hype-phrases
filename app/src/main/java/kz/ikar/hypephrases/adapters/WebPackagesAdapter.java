package kz.ikar.hypephrases.adapters;

import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.models.Pack;

/**
 * Created by User on 23.11.2017.
 */

public class WebPackagesAdapter extends RecyclerView.Adapter<WebPackagesAdapter.WebPackagesViewHolder> {
    private List<Pack> mListItem;
    private StorageReference storageReference;
    private OnClickListener listener;

    public enum ActionType {
        Download,
        Remove
    }

    public interface OnClickListener {
        void OnDownloadClicked(Pack pack, ActionType actionType);
    }

    public WebPackagesAdapter(List<Pack> mListItem, StorageReference storageReference, OnClickListener listener) {
        this.mListItem = mListItem;
        this.storageReference = storageReference;
        this.listener = listener;
    }

    @Override
    public WebPackagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_package, parent, false);

        return new WebPackagesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WebPackagesViewHolder holder, int position) {
        holder.bind(mListItem.get(position), storageReference, listener, position);
    }

    @Override
    public int getItemCount() {
        if (mListItem == null)
            return 0;

        return mListItem.size();
    }

    public static class WebPackagesViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvCost;
        private Button btnDownload;
        private ImageView ivLogo;
        private ProgressBar progressBar;

        //private int lastPosition = -1;
        private final static int FADE_DURATION = 1000;

        public WebPackagesViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvCost = (TextView) itemView.findViewById(R.id.tv_cost);
            btnDownload = (Button) itemView.findViewById(R.id.btn_download);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
        }

        public void bind(final Pack pack, StorageReference storageReference, final OnClickListener listener, int position) {
            tvName.setText(pack.getName());
            tvCost.setText(pack.getCost() + " монет");

            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnDownload.getText().equals("Скачать")) {
                        listener.OnDownloadClicked(pack, ActionType.Download);
                    } else if (btnDownload.getText().equals("Удалить")) {
                        listener.OnDownloadClicked(pack, ActionType.Remove);
                    }
                }
            });

            if (DBHelper.checkIfPackExists(pack)) {
                btnDownload.setText("Удалить");
                /*btnDownload.setCompoundDrawablesWithIntrinsicBounds(null,
                        ContextCompat.getDrawable(btnDownload.getContext(), R.drawable.ic_close_white),
                        null,
                        null);*/

                pack.setLogo(DBHelper.getLogoOfPackWithId(pack.getId()));
                File imgFile = new File(pack.getLogo());
                Picasso.with(tvName.getContext())
                        .load(imgFile)
                        .resize(128, 128)
                        .centerCrop()
                        .into(ivLogo);

                progressBar.setVisibility(View.GONE);
                ivLogo.setVisibility(View.VISIBLE);
            } else {
                btnDownload.setText("Скачать");
                /*btnDownload.setCompoundDrawablesWithIntrinsicBounds(null,
                        ContextCompat.getDrawable(btnDownload.getContext(), R.drawable.ic_file_download),
                        null,
                        null);*/

                storageReference.child("logos/" + pack.getLogo()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        pack.setLogo(uri.toString());
                        Picasso.with(tvName.getContext())
                                .load(pack.getLogo())
                                .resize(128, 128)
                                .centerCrop()
                                .into(ivLogo);

                        progressBar.setVisibility(View.GONE);
                        ivLogo.setVisibility(View.VISIBLE);
                    }
                });
            }

            //setAnimation(itemView, position);
            //setFadeAnimation(itemView);
        }

        /*private void setFadeAnimation(View view) {
            AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(FADE_DURATION);
            view.startAnimation(anim);
        }*/

        /*private void setAnimation(View viewToAnimate, int position)
        {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > -1)
            {
                Animation animation = AnimationUtils.loadAnimation(tvName.getContext(), android.R.anim.slide_in_left);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }*/
    }
}
