package kz.ikar.hypephrases.adapters;

import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.models.Phrase;

/**
 * Created by User on 20.09.2017.
 */

public class PhrasesAdapter extends RecyclerView.Adapter<PhrasesAdapter.PhrasesViewHolder> {
    private List<Phrase> itemList;
    private OnPhraseClickListener listener;

    public interface OnPhraseClickListener {
        void onItemClicked(int position);
        void onShareButtonClicked(int position);
    }

    public PhrasesAdapter(List<Phrase> itemList, OnPhraseClickListener listener) {
        this.itemList = itemList;
        this.listener = listener;
    }

    @Override
    public PhrasesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_phrase, parent, false);


        return new PhrasesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhrasesViewHolder holder, int position) {
        holder.bind(itemList.get(position), listener, position);
    }

    @Override
    public int getItemCount() {
        if (itemList == null)
            return 0;

        return itemList.size();
    }

    public static class PhrasesViewHolder extends RecyclerView.ViewHolder {
        TextView tvPhrase;
        ImageView ivShare;
        //CardView cvTile;

        public PhrasesViewHolder(View itemView) {
            super(itemView);

            tvPhrase = (TextView) itemView.findViewById(R.id.tv_phrase);
            ivShare = (ImageView) itemView.findViewById(R.id.iv_share);
            //cvTile = (CardView) itemView.findViewById(R.id.cv_tile);
        }

        public void bind(Phrase phrase, final OnPhraseClickListener listener, final int position) {
            /*Typeface myTypeface = Typeface.createFromAsset(tvPhrase.getContext().getAssets(),
                    "fonts/TitilliumWebLight.ttf");

            tvPhrase.setTypeface(myTypeface);*/
            tvPhrase.setText(phrase.getText());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(position);
                }
            });

            ivShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onShareButtonClicked(position);
                }
            });
            /*cvTile.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onShareButtonClicked(position);
                    return true;
                }
            });*/
        }
    }
}
