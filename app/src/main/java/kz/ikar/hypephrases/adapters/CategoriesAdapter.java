package kz.ikar.hypephrases.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.models.Category;
import kz.ikar.hypephrases.models.Phrase;

/**
 * Created by User on 18.12.2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {
    private List<Category> mItemList;
    private OnClickListener listener;

    public interface OnClickListener {
        void onItemClicked(Category category);
    }

    public CategoriesAdapter(List<Category> mItemList, OnClickListener listener) {
        this.mItemList = mItemList;
        this.listener = listener;
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);

        return new CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, int position) {
        holder.bind(mItemList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        if (mItemList == null)
            return 0;

        return mItemList.size();
    }

    public static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        Button btnCategory;

        public CategoriesViewHolder(View itemView) {
            super(itemView);

            btnCategory = (Button) itemView.findViewById(R.id.btn_category);
        }

        public void bind(final Category category, final OnClickListener listener) {
            btnCategory.setText(category.getName());

            btnCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(category);
                }
            });
        }
    }
}
