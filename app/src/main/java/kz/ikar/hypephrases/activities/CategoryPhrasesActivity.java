package kz.ikar.hypephrases.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.adapters.SearchPhrasesAdapter;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.models.AppContext;
import kz.ikar.hypephrases.models.Phrase;

public class CategoryPhrasesActivity extends AppCompatActivity {
    private RecyclerView rvPhrases;
    private CardView cvEmpty;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_phrases);

        String category = getIntent().getStringExtra("CATEGORY");

        rvPhrases = (RecyclerView) findViewById(R.id.rv_phrases);
        cvEmpty = (CardView) findViewById(R.id.cv_empty);
        btnBack = (Button) findViewById(R.id.btn_back);

        rvPhrases.setLayoutManager(new GridLayoutManager(this, 1));
        SearchPhrasesAdapter adapter = new SearchPhrasesAdapter(DBHelper.getPhrasesByCategory(category), new SearchPhrasesAdapter.OnClickListener() {
            @Override
            public void OnItemClicked(Phrase phrase) {
                try {
                    MediaPlayer player = AppContext.player;
                    player.reset();
                    player.setDataSource(DBHelper.rootDir + phrase.getFilename());
                    player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                    player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                        }
                    });
                    player.prepareAsync();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnItemSended(Phrase phrase) {
                Uri uri = Uri.parse(DBHelper.rootDir + phrase.getFilename());
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("audio/*");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(share, "Отправка фразы..."));
            }
        });
        if (adapter.getItemCount() != 0) {
            rvPhrases.setAdapter(adapter);
        } else {
            rvPhrases.setVisibility(View.GONE);
            cvEmpty.setVisibility(View.VISIBLE);
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        setTitle(category);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }
}
