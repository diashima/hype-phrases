package kz.ikar.hypephrases.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.fragments.LocalFragment;
import kz.ikar.hypephrases.fragments.NetworkFragment;
import kz.ikar.hypephrases.models.AppContext;

public class MainActivity extends AppCompatActivity {
    private SearchView searchView;
    public BottomNavigationView bottomNavigationView;
    public FirebaseDatabase database;

    public static final int READ_EXTERNAL_STORAGE_PERMISSION = 250;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_main);

        askForPermissions();
        initHelpers();
        initUIVariables();
        moveTo(R.id.action_local);
    }

    public void moveTo(int id) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = null;

        switch (id) {
            case R.id.action_local:
                fragment = new LocalFragment();
                break;
            case R.id.action_network:
                fragment = new NetworkFragment();
                break;
        }

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.content_main, fragment, "Current");
        transaction.commit();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void askForPermissions() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(bottomNavigationView, "Доступ к хранилищу нужен для сохранения фраз :(", Toast.LENGTH_LONG)
                        .setAction("Да", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                askForPermissions();
                            }
                        }).show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_EXTERNAL_STORAGE_PERMISSION);
            }
        }
    }

    private void initHelpers() {
        //MobileAds.initialize(this, getString(R.string.admob_app_id));
        //mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        //AppContext.Vungle.init(this, VUNGLE_APP_ID, new String[] { VUNGLE_PLACEMENT_ID }, );
        AppContext.initializePlayer(new MediaPlayer(), getApplicationContext());
        FirebaseApp.initializeApp(this);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        DBHelper.loadDaoSession(getApplicationContext());

        SharedPreferences sPref = getPreferences(MODE_PRIVATE);
        long coins = sPref.getLong("COINS", -305);
        if (coins == -305) {
            sPref.edit().putLong("COINS", 20).apply();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppContext.Vungle.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppContext.Vungle.onPause();
    }

    @Override
    protected void onDestroy() {
        AppContext.Vungle.clearEventListeners();
        super.onDestroy();
    }

    private void initUIVariables() {
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_view);

        setTitle("Хайповые фразы");

        database = FirebaseDatabase.getInstance();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (bottomNavigationView.getSelectedItemId() != item.getItemId()) {
                    moveTo(item.getItemId());
                }
                return true;
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.action_local);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.menu.main_menu:

                //drawerLayout.openDrawer(Gravity.START);
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
            }
        }
    }

    /*private void loadFolders() {
        AssetManager assetManager = getAssets();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(assetManager.open("phrases/authors.txt"))
            );

            String mLine;

            people = new ArrayList<>();

            while ((mLine = reader.readLine()) != null) {
                String[] strs = mLine.split(";");
                people.add(new Author(strs[0], strs[1],
                        assetManager.list("phrases/" + strs[0]).length - 2,
                        getBitmapFromAssets("phrases/" + strs[0] + "/logo.png")));
            }

            /*String[] files = assetManager.list("phrases");
            people = new ArrayList<>();
            for (String str : files) {
                people.add(new Author(str,
                        assetManager.list("phrases/" + str).length - 2,
                        getBitmapFromAssets("phrases/" + str + "/logo.png")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromAssets(String fileName){
        AssetManager am = getAssets();
        InputStream is = null;

        try{
            is = am.open(fileName);
        } catch (IOException e){
            e.printStackTrace();
        }

        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }*/
}
