package kz.ikar.hypephrases.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.adapters.PhrasesAdapter;

public class PersonActivity extends AppCompatActivity {
    private String personName;
    private String personFolderName;

    private RecyclerView rvPhrases;
    private List<String> phrases;
    private MediaPlayer player;
    //private NestedScrollView svPerson;

    public static final int WRITE_EXTERNAL_STORAGE_PERMISSION = 200;
    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        //player = MainActivity.player;

        rvPhrases = (RecyclerView) findViewById(R.id.rv_phrases);
        rvPhrases.setLayoutManager(new GridLayoutManager(this, 1));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(),
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.custom_divider));
        rvPhrases.addItemDecoration(dividerItemDecoration);

        personName = getIntent().getStringExtra("personName");
        personFolderName = getIntent().getStringExtra("personFolderName");
        setTitle(personName);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        readFromFile();

        /*if (phrases != null) {
            PhrasesAdapter adapter = new PhrasesAdapter(phrases, new PhrasesAdapter.OnPhraseClickListener() {
                @Override
                public void onItemClicked(int position) {
                    if (player != null) {
                        player.release();
                    }
                    MainActivity.player = new MediaPlayer();
                    player = MainActivity.player;
                    try {
                        AssetManager am = getAssets();
                        AssetFileDescriptor assetFileDescriptor = am.openFd("phrases/" + personFolderName + "/" + position + ".mp3");
                        player.setDataSource(assetFileDescriptor.getFileDescriptor(),
                                assetFileDescriptor.getStartOffset(),
                                assetFileDescriptor.getLength());
                        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                            }
                        });
                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                        player.prepareAsync();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onShareButtonClicked(int position) {
                    if (ContextCompat.checkSelfPermission(PersonActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        PersonActivity.this.position = position;
                        if (ActivityCompat.shouldShowRequestPermissionRationale(PersonActivity.this,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        } else {
                            ActivityCompat.requestPermissions(PersonActivity.this,
                                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    READ_EXTERNAL_STORAGE_PERMISSION);
                        }
                    } else {
                        sendFile("phrases/" + personName + "/" + position + ".mp3");
                    }
                }
            });
            rvPhrases.setAdapter(adapter);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendFile("phrases/" + personFolderName + "/" + position + ".mp3");
                } else {

                }
            }
        }
    }

    private void readFromFile() {
        phrases = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("phrases/" + personFolderName + "/phrases.txt"), "UTF-8"));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                phrases.add(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    private void sendFile(String filePath) {
        prepareForShare(filePath);
        Uri uri = Uri.parse("file://" + Environment.getExternalStorageDirectory()
                + "/sound.mp3");
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("audio/*");
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(share, "Phrase sharing..."));
    }

    private File prepareForShare(String filePath) {
        InputStream in = null;
        OutputStream out = null;
        File outFile;
        try {
            in = getAssets().open(filePath);

            outFile = new File(Environment.getExternalStorageDirectory() + "/sound.mp3");
            out = new FileOutputStream(outFile);

            copyFile(in, out);
            in.close();
            in = null;

            out.flush();
            out.close();
            out = null;

            return outFile;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
