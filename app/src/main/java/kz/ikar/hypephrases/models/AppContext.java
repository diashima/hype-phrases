package kz.ikar.hypephrases.models;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.Toast;

import com.vungle.publisher.VungleInitListener;
import com.vungle.publisher.VunglePub;

/**
 * Created by User on 28.11.2017.
 */

public class AppContext {
    public static MediaPlayer player;
    public static final VunglePub Vungle = VunglePub.getInstance();

    private static final String VUNGLE_APP_ID = "5a5475f56f858d3b0c01224c";
    public static final String VUNGLE_PLACEMENT_ID = "DEFAULT16679";

    public static void initializePlayer(MediaPlayer player, final Context context) {
        AppContext.player = player;

        AppContext.Vungle.init(context, VUNGLE_APP_ID, new String[]{VUNGLE_PLACEMENT_ID}, new VungleInitListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(context, "Add successfully initialized", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable throwable) {
                Toast.makeText(context, "An issue occurred", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
