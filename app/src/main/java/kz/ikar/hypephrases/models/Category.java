package kz.ikar.hypephrases.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 17.12.2017.
 */
@Entity
public class Category {
    @Id
    private long id;
    @NotNull
    private String name;

    public Category() {
    }

    @Generated(hash = 1778992104)
    public Category(long id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
