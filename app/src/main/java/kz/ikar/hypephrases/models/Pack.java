package kz.ikar.hypephrases.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Transient;

/**
 * Created by User on 23.11.2017.
 */
@Entity
public class Pack {
    @Id
    private long id;
    @NotNull
    private String name;
    private String logo;
    @NotNull
    @ToMany(referencedJoinProperty = "packId")
    private List<Phrase> phrases;
    @NotNull
    private long cost;
    @NotNull
    private long updatedAt;
    @Transient
    public boolean isReady = false;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 402913394)
    private transient PackDao myDao;

    public Pack() {
    }

    public Pack(long id, String name, String logo, List<Phrase> phrases, long cost, long updatedAt) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.phrases = phrases;
        this.cost = cost;
        this.updatedAt = updatedAt;
    }

    @Generated(hash = 1331878360)
    public Pack(long id, @NotNull String name, String logo, long cost, long updatedAt) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.cost = cost;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setPhrases(List<Phrase> phrases) {
        this.phrases = phrases;
    }
/*public void putPhrase(Phrase phrase) {
        if (phrases == null) {
            phrases = new ArrayList<>();
        }

        phrases.add(phrase);
    }*/

    public List<Phrase> getAllPhrases() {
        return phrases;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public Date getUpdatedAtTime() {
        Locale locale = new Locale("ru", "RU");
        Calendar cal = Calendar.getInstance(locale);
        cal.setTimeInMillis(updatedAt);
        return cal.getTime();
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 301428872)
    public List<Phrase> getPhrases() {
        if (phrases == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PhraseDao targetDao = daoSession.getPhraseDao();
            List<Phrase> phrasesNew = targetDao._queryPack_Phrases(id);
            synchronized (this) {
                if (phrases == null) {
                    phrases = phrasesNew;
                }
            }
        }
        return phrases;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 2060979551)
    public synchronized void resetPhrases() {
        phrases = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1642859777)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getPackDao() : null;
    }
}
