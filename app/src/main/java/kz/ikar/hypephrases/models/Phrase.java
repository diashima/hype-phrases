package kz.ikar.hypephrases.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by User on 26.11.2017.
 */

@Entity
public class Phrase {
    @Id
    private long id;
    @NotNull
    private String text;
    @NotNull
    private String filename;
    @NotNull
    private long packId;
    private String categories;

    public Phrase(long id, String text, String filename, String categories) {
        this.id = id;
        this.text = text;
        this.filename = filename;
        this.categories = categories;
    }
    @Generated(hash = 2041825758)
    public Phrase(long id, @NotNull String text, @NotNull String filename,
            long packId, String categories) {
        this.id = id;
        this.text = text;
        this.filename = filename;
        this.packId = packId;
        this.categories = categories;
    }
    @Generated(hash = 1136898449)
    public Phrase() {
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getPackId() {
        return packId;
    }

    public void setPackId(long packId) {
        this.packId = packId;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }
}
