package kz.ikar.hypephrases.models;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "PHRASE".
*/
public class PhraseDao extends AbstractDao<Phrase, Long> {

    public static final String TABLENAME = "PHRASE";

    /**
     * Properties of entity Phrase.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, long.class, "id", true, "_id");
        public final static Property Text = new Property(1, String.class, "text", false, "TEXT");
        public final static Property Filename = new Property(2, String.class, "filename", false, "FILENAME");
        public final static Property PackId = new Property(3, long.class, "packId", false, "PACK_ID");
        public final static Property Categories = new Property(4, String.class, "categories", false, "CATEGORIES");
    }

    private Query<Phrase> pack_PhrasesQuery;

    public PhraseDao(DaoConfig config) {
        super(config);
    }
    
    public PhraseDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"PHRASE\" (" + //
                "\"_id\" INTEGER PRIMARY KEY NOT NULL ," + // 0: id
                "\"TEXT\" TEXT NOT NULL ," + // 1: text
                "\"FILENAME\" TEXT NOT NULL ," + // 2: filename
                "\"PACK_ID\" INTEGER NOT NULL ," + // 3: packId
                "\"CATEGORIES\" TEXT);"); // 4: categories
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"PHRASE\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Phrase entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getId());
        stmt.bindString(2, entity.getText());
        stmt.bindString(3, entity.getFilename());
        stmt.bindLong(4, entity.getPackId());
 
        String categories = entity.getCategories();
        if (categories != null) {
            stmt.bindString(5, categories);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Phrase entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getId());
        stmt.bindString(2, entity.getText());
        stmt.bindString(3, entity.getFilename());
        stmt.bindLong(4, entity.getPackId());
 
        String categories = entity.getCategories();
        if (categories != null) {
            stmt.bindString(5, categories);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    @Override
    public Phrase readEntity(Cursor cursor, int offset) {
        Phrase entity = new Phrase( //
            cursor.getLong(offset + 0), // id
            cursor.getString(offset + 1), // text
            cursor.getString(offset + 2), // filename
            cursor.getLong(offset + 3), // packId
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4) // categories
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Phrase entity, int offset) {
        entity.setId(cursor.getLong(offset + 0));
        entity.setText(cursor.getString(offset + 1));
        entity.setFilename(cursor.getString(offset + 2));
        entity.setPackId(cursor.getLong(offset + 3));
        entity.setCategories(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Phrase entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Phrase entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Phrase entity) {
        throw new UnsupportedOperationException("Unsupported for entities with a non-null key");
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "phrases" to-many relationship of Pack. */
    public List<Phrase> _queryPack_Phrases(long packId) {
        synchronized (this) {
            if (pack_PhrasesQuery == null) {
                QueryBuilder<Phrase> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.PackId.eq(null));
                pack_PhrasesQuery = queryBuilder.build();
            }
        }
        Query<Phrase> query = pack_PhrasesQuery.forCurrentThread();
        query.setParameter(0, packId);
        return query.list();
    }

}
