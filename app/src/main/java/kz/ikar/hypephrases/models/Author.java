package kz.ikar.hypephrases.models;

import android.graphics.Bitmap;

/**
 * Created by User on 19.09.2017.
 */

public class Author {
    private String folderName;
    private String name;
    private int phrasesCount;
    private Bitmap logo;

    public Author(String folderName, String name, int phrasesCount, Bitmap logo) {
        this.folderName = folderName;
        this.name = name;
        this.phrasesCount = phrasesCount;
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhrasesCount() {
        return phrasesCount;
    }

    public void setPhrasesCount(int phrasesCount) {
        this.phrasesCount = phrasesCount;
    }

    public Bitmap getLogo() {
        return logo;
    }

    public void setLogo(Bitmap logo) {
        this.logo = logo;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }
}
