package kz.ikar.hypephrases.fragments;


import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.vungle.publisher.VungleAdEventListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.activities.MainActivity;
import kz.ikar.hypephrases.adapters.WebPackagesAdapter;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.models.AppContext;
import kz.ikar.hypephrases.models.Pack;
import kz.ikar.hypephrases.models.Phrase;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetworkFragment extends Fragment {
    private DatabaseReference databaseReference;
    private StorageReference storageReference;
    private FirebaseAuth fAuth;

    private RecyclerView rvNames;
    private CardView cvEmpty;
    private CardView cvCoins;
    private ProgressBar pbLoadingWebPackages;
    private Button btnTryAgain;
    private TextView tvCoins;
    private Button btnGetCoins;

    private List<Pack> packs;
    private Pack downloadingPack;

    private long coins;

    public final VungleAdEventListener vungleAdEventListener = new VungleAdEventListener() {
        @Override
        public void onAdEnd(@NonNull String s, boolean b, boolean b1) {
            Log.e("HYPEPHRASES", "onAdEnd");

            if (b) {
                increaseCoins(15);
            }
        }

        @Override
        public void onAdStart(@NonNull String s) {
            Log.e("HYPEPHRASES", "onAdStart");
        }

        @Override
        public void onUnableToPlayAd(@NonNull String s, String s1) {
            Log.e("HYPEPHRASES", "onUnableToPlayAd");
        }

        @Override
        public void onAdAvailabilityUpdate(@NonNull String s, boolean b) {
            Log.e("HYPEPHRASES", "onAdAvailabilityUpdate");
        }


    };

    enum FileType {
        Audio,
        Image
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //DBHelper.removeAllPacks();

        View view = inflater.inflate(R.layout.fragment_network, container, false);

        rvNames = (RecyclerView) view.findViewById(R.id.rv_names);
        cvEmpty = (CardView) view.findViewById(R.id.cv_empty);
        pbLoadingWebPackages = (ProgressBar) view.findViewById(R.id.pb_loading_web_packages);
        btnTryAgain = (Button) view.findViewById(R.id.btn_try_again);
        tvCoins = (TextView) view.findViewById(R.id.tv_coins);
        cvCoins = (CardView) view.findViewById(R.id.cv_coins);
        btnGetCoins = (Button) view.findViewById(R.id.btn_get_coins);

        rvNames.setLayoutManager(new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false));

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).moveTo(R.id.action_network);
            }
        });

        final SharedPreferences sPref = getActivity().getPreferences(MODE_PRIVATE);
        coins = sPref.getLong("COINS", 0);
        tvCoins.setText("" + coins);

        btnGetCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppContext.Vungle != null && AppContext.Vungle.isAdPlayable(AppContext.VUNGLE_PLACEMENT_ID)) {
                    if (sPref.getBoolean("ShowMessage", true)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                        builder.setMessage("Для получения монет, вам лишь необходимо просмотреть видеоролик :)");
                        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                AppContext.Vungle.playAd(AppContext.VUNGLE_PLACEMENT_ID, null);
                            }
                        });
                        builder.setNegativeButton("Не напоминать", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sPref.edit().putBoolean("ShowMessage", false).apply();
                                dialog.dismiss();
                                AppContext.Vungle.playAd(AppContext.VUNGLE_PLACEMENT_ID, null);
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        AppContext.Vungle.playAd(AppContext.VUNGLE_PLACEMENT_ID, null);
                    }
                } else {
                    Toast.makeText(getActivity(), "Проверьте доступ к интернету и попробуйте еще раз :)", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        packs = new ArrayList<>();

        fAuth = FirebaseAuth.getInstance();
        //signInAnonymously();
        databaseReference = ((MainActivity)getActivity()).database.getReference();
        storageReference = FirebaseStorage.getInstance().getReference();
        loadWebPackages();

        loadRewardedVideoAd();
    }

    private void signInAnonymously() {
        fAuth.signInAnonymously()
                .addOnSuccessListener(getActivity(), new  OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        // do your stuff
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("AUTH", "signInAnonymously:FAILURE", exception);
                    }
                });
    }

    private void loadWebPackages() {
        packs = new ArrayList<>();
        if (!isOnline()) {
            pbLoadingWebPackages.setVisibility(View.GONE);
            cvEmpty.setVisibility(View.VISIBLE);
            return;
        }
        databaseReference.child("packages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                packs = new ArrayList<>();
                for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    final Pack pack = singleSnapshot.getValue(Pack.class);
                    if (pack != null && pack.isReady) {
                        packs.add(pack);

                        updateRecyclerView();
                    }
                    //Snackbar.make(view, pack.getName(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Snackbar.make(view, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadRewardedVideoAd() {
        if (AppContext.Vungle != null && AppContext.Vungle.isInitialized()) {
            AppContext.Vungle.clearAndSetEventListeners(vungleAdEventListener);
            AppContext.Vungle.loadAd(AppContext.VUNGLE_PLACEMENT_ID);
        }
    }

    private void updateRecyclerView() {
        if (!isOnline()) {
            pbLoadingWebPackages.setVisibility(View.GONE);
            cvEmpty.setVisibility(View.VISIBLE);
            rvNames.setVisibility(View.GONE);
        } else {
            WebPackagesAdapter adapter = new WebPackagesAdapter(packs, storageReference, new WebPackagesAdapter.OnClickListener() {
                @Override
                public void OnDownloadClicked(Pack pack, WebPackagesAdapter.ActionType actionType) {
                    if (actionType == WebPackagesAdapter.ActionType.Download) {
                        if (coins < pack.getCost()) {
                            ObjectAnimator
                                    .ofFloat(cvCoins, "translationX", 0, 25, -25, 25, -25,15, -15, 6, -6, 0)
                                    .setDuration(1000)
                                    .start();
                            Snackbar.make(rvNames, "У вас недостаточно монет для данного пака :(", 3000).show();
                        } else {
                            Toast.makeText(getActivity(), "Скачиваем...", Toast.LENGTH_SHORT).show();
                            downloadPack(pack.getId());
                            decreaseCoins(pack.getCost());
                        }
                    } else if (actionType == WebPackagesAdapter.ActionType.Remove) {
                        Toast.makeText(getActivity(), "Удаляем...", Toast.LENGTH_SHORT).show();
                        removePack(pack.getId());
                    }
                }
            });
            rvNames.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            rvNames.getViewTreeObserver().removeOnPreDrawListener(this);

                            for (int i = 0; i < rvNames.getChildCount(); i++) {
                                View v = rvNames.getChildAt(i);
                                v.setAlpha(0.0f);
                                v.animate().alpha(1.0f)
                                        .setDuration(300)
                                        .setStartDelay(i * 50)
                                        .start();
                            }

                            return true;
                        }
                    });

            pbLoadingWebPackages.setVisibility(View.GONE);
            cvEmpty.setVisibility(View.GONE);
            rvNames.setVisibility(View.VISIBLE);
            rvNames.setAdapter(adapter);
        }
    }

    private boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void decreaseCoins(long value) {
        coins -= value;
        SharedPreferences sPrefs = getActivity().getPreferences(MODE_PRIVATE);
        sPrefs.edit().putLong("COINS", coins).apply();
        updateCoinsView(coins + value);
    }

    private void increaseCoins(long value) {
        coins += value;
        SharedPreferences sPrefs = getActivity().getPreferences(MODE_PRIVATE);
        sPrefs.edit().putLong("COINS", coins).apply();
        updateCoinsView(coins - value);
    }

    private void updateCoinsView(final long oldValue) {
        Handler handler = new Handler(getActivity().getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                startCountAnimation(oldValue);
            }
        });
    }

    private void startCountAnimation(long oldValue) {
        ValueAnimator animator = ValueAnimator.ofInt((int)oldValue, (int)coins);
        animator.setDuration(5000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                tvCoins.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }

    private void removePack(long packageId) {
        DBHelper.removePack(packageId);
        loadWebPackages();
    }

    private void downloadPack(final long packageId) {
        final Query downloadPackQuery = databaseReference.child("packages").orderByChild("id").equalTo(packageId);

        downloadPackQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    downloadingPack = singleSnapshot.getValue(Pack.class);
                    downloadFile(downloadingPack.getLogo(), FileType.Image);
                    downloadingPack.setLogo(DBHelper.rootDir + downloadingPack.getLogo());
                }

                Query downloadPhrasesQuery = databaseReference.child("phrases").orderByChild("packId").equalTo(downloadingPack.getId());

                downloadPhrasesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Phrase phrase = singleSnapshot.getValue(Phrase.class);
                            downloadFile(phrase.getFilename(), FileType.Audio);
                            DBHelper.addPhrase(phrase);
                        }
                        DBHelper.addPack(downloadingPack);
                        loadWebPackages();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void downloadFile(String filename, FileType type) {
        String rootFolder;
        if (type == FileType.Audio) {
            rootFolder = "phrases";
        } else {
            rootFolder = "logos";
        }

        File rootDir = new File(DBHelper.rootDir);
        if(!rootDir.exists()) {
            rootDir.mkdirs();
        }
        File file = null;
        try {
            file = new File(rootDir, filename);
            file.createNewFile();

            storageReference.child(rootFolder).child(filename).getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Log.i("HYPEPHRASES", "DOWNLOADED SUCCESSFULLY" + taskSnapshot.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("HYPEPHRASES", "DOWNLOADED WITH ERROR" + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void checkLocalFiles() {
        File file = new File(DBHelper.rootDir);
        String[] files = file.list();
        return;
    }
}
