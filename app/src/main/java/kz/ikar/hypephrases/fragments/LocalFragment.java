package kz.ikar.hypephrases.fragments;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import kz.ikar.hypephrases.R;
import kz.ikar.hypephrases.activities.CategoryPhrasesActivity;
import kz.ikar.hypephrases.activities.MainActivity;
import kz.ikar.hypephrases.adapters.CategoriesAdapter;
import kz.ikar.hypephrases.adapters.PackagesAdapter;
import kz.ikar.hypephrases.adapters.SearchPhrasesAdapter;
import kz.ikar.hypephrases.db.DBHelper;
import kz.ikar.hypephrases.models.Category;
import kz.ikar.hypephrases.models.AppContext;
import kz.ikar.hypephrases.models.Pack;
import kz.ikar.hypephrases.models.Phrase;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocalFragment extends Fragment {
    private RecyclerView rvPackages;
    private RecyclerView rvCategories;
    private NestedScrollView svMain;
    private SearchView searchView;
    private CardView cvEmpty;
    private Button btnMoveToWeb;

    private List<Pack> packages;
    private DatabaseReference databaseReference;

    public LocalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_local, container, false);

        initUIVariables(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setViewCinfigurations();
        prepareRecyclerView();
        //loadFolders();
        loadLocalPackages();
        updateRecyclerView();
        downloadAndUpdateCategories();
    }

    private void downloadAndUpdateCategories() {
        databaseReference.child("categories").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Category> categories = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    categories.add(snapshot.getValue(Category.class));
                }
                DBHelper.updateCategories(categories);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initUIVariables(View view) {
        svMain = (NestedScrollView) view.findViewById(R.id.sv_main);
        rvPackages = (RecyclerView) view.findViewById(R.id.rv_names);
        cvEmpty = (CardView) view.findViewById(R.id.cv_empty);
        btnMoveToWeb = (Button) view.findViewById(R.id.btn_move_to_web);
        rvCategories = (RecyclerView) view.findViewById(R.id.rv_categories);

        btnMoveToWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).bottomNavigationView.setSelectedItemId(R.id.action_network);
            }
        });

        databaseReference = ((MainActivity)getActivity()).database.getReference();
    }

    private void prepareRecyclerView() {
        rvPackages.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_divider));
        rvPackages.addItemDecoration(dividerItemDecoration);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvCategories.setLayoutManager(layoutManager);
    }

    private void setViewCinfigurations() {
        //setTitle("Хайповые фразы");
        setHasOptionsMenu(true);
        svMain.smoothScrollTo(0, 0);
    }

    private void updateRecyclerView() {
        PackagesAdapter adapter = new PackagesAdapter(packages, getActivity().getAssets(), (MainActivity) getActivity(), new PackagesAdapter.OnClickListener() {
            @Override
            public void onPackDeleteClicked(Pack pack) {
                Toast.makeText(getActivity(), "Удаляем...", Toast.LENGTH_SHORT).show();
                DBHelper.removePack(pack.getId());
                loadLocalPackages();
                updateRecyclerView();
            }
        });
        if (adapter.getItemCount() == 0) {
            cvEmpty.setVisibility(View.VISIBLE);
            rvCategories.setVisibility(View.GONE);
            rvPackages.setVisibility(View.GONE);
        } else {
            cvEmpty.setVisibility(View.GONE);
            CategoriesAdapter categoriesAdapter = new CategoriesAdapter(DBHelper.getCategories(), new CategoriesAdapter.OnClickListener() {
                @Override
                public void onItemClicked(Category category) {
                    Intent intent = new Intent(getContext(), CategoryPhrasesActivity.class);
                    intent.putExtra("CATEGORY", category.getName());
                    startActivity(intent);
                }
            });
            rvCategories.setAdapter(categoriesAdapter);
            rvCategories.setVisibility(View.VISIBLE);
            rvPackages.setAdapter(adapter);
            rvPackages.setVisibility(View.VISIBLE);
        }
    }

    private void loadLocalPackages() {
        packages = DBHelper.getPacks();
        for (Pack pack: packages) {
            pack.getPhrases();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        SearchManager searchManager = (SearchManager) rvPackages.getContext().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.isEmpty()) {
                    SearchPhrasesAdapter adapter = new SearchPhrasesAdapter(DBHelper.getPhrases(newText), new SearchPhrasesAdapter.OnClickListener() {
                        @Override
                        public void OnItemClicked(Phrase phrase) {
                            try {
                                MediaPlayer player = AppContext.player;
                                player.reset();
                                player.setDataSource(DBHelper.rootDir + phrase.getFilename());
                                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mp.start();
                                    }
                                });
                                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        mp.reset();
                                    }
                                });
                                player.prepareAsync();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void OnItemSended(Phrase phrase) {
                            Uri uri = Uri.parse(DBHelper.rootDir + phrase.getFilename());
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("audio/*");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            rvPackages.getContext().startActivity(Intent.createChooser(share, "Отправка фразы..."));
                        }
                    });
                    rvCategories.setVisibility(View.GONE);
                    rvPackages.setAdapter(adapter);
                    return true;
                } else {
                    rvCategories.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });
        MenuItem item = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                updateRecyclerView();
                return true;
            }
        });
        /*searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                updateRecyclerView();

                return true;
            }
        });*/

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
        }

        return false;
    }

    /*private void loadFolders() {
        AssetManager assetManager = getActivity().getAssets();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(assetManager.open("phrases/authors.txt"))
            );

            String mLine;

            packages = new ArrayList<>();

            while ((mLine = reader.readLine()) != null) {
                String[] strs = mLine.split(";");
                packages.add(new Author(strs[0], strs[1],
                        assetManager.list("phrases/" + strs[0]).length - 2,
                        getBitmapFromAssets("phrases/" + strs[0] + "/logo.png")));
            }

            /*String[] files = assetManager.list("phrases");
            people = new ArrayList<>();
            for (String str : files) {
                people.add(new Author(str,
                        assetManager.list("phrases/" + str).length - 2,
                        getBitmapFromAssets("phrases/" + str + "/logo.png")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    private Bitmap getBitmapFromAssets(String fileName){
        AssetManager am = getActivity().getAssets();
        InputStream is = null;

        try{
            is = am.open(fileName);
        } catch (IOException e){
            e.printStackTrace();
        }

        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }
}
